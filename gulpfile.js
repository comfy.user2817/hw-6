import gulp from 'gulp';

const {src, dest, watch, series, parallel} = gulp;

import htmlmin from 'gulp-htmlmin';


import imagemin from 'gulp-imagemin';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';


const sass = gulpSass(dartSass);

import autoprefixer from 'gulp-autoprefixer';



const htmlTask = () => {
    return src('./src/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(dest('./dist/'))
}


const scssTask = () => {
    return src('./src/styles/style.scss')
    .pipe(sass({outputStyle: 'compressed'}).on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(dest('./dist/css'))
}

const imgTask = () => {
    return src('./src/img/**/*.*')
    //.pipe(imagemin({ verbose: true, quality: 90 }))
    .pipe(dest("./dist/img"));
}

export const html = htmlTask;
export const css = scssTask;
export const img = imgTask;

